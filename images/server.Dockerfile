FROM docker.io/golang:1.19 as builder

WORKDIR /usr/src/chaton

COPY ./server ./server
COPY ./grpc ./grpc
COPY go.mod .
COPY go.sum .

WORKDIR /usr/src/chaton/server

RUN CGO_ENABLED=0 go build -o /bin/server


FROM docker.io/alpine

WORKDIR /bin/chaton

COPY --from=builder /bin/server .

CMD ["./server"]
